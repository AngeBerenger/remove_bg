# Background remover: A streamlit application that allows you to remove the background from a photo

# IMPORT
import streamlit as st
from PIL import Image
from rembg import remove
from io import BytesIO
import os
import time

# Page config
st.set_page_config(page_title="Background remover", page_icon="✏️", layout="wide")
st.write("# Remove Image Background")
st.write("### 100% Automatically and **Free**")
st.sidebar.write("### Upload Image")
col1, col2 = st.columns(2)


def contert_to_png(image):
    btio = BytesIO()
    image.save(btio, format="PNG")
    my_image = btio.getvalue()
    return my_image


def default_load(image):
    msg = st.toast('Default image 😊...')
    time.sleep(1)
    msg.toast('Download ⌛...')
    time.sleep(2)
    msg.toast('Ready!', icon="🥞")
    rm_bg(image)


def rm_bg(image):
    im = Image.open(image)
    col1.write("Original Image")

    if isinstance(image, str):
        file_name = os.path.splitext(os.path.basename(image))[0]
    else:
        file_name = os.path.splitext(os.path.basename(image.name))[0]

    col1.write(f"Name: {file_name}")
    col1.image(im)

    with st.spinner('Removing background... ⌛'):
        img_rm = remove(im)
        img_rm_dow = contert_to_png(img_rm)

    col2.write("Background Removed")
    col2.write(f"Name: {file_name}_rembg")
    col2.image(img_rm)

    st.sidebar.markdown("\n")

    st.toast('Your edited image was saved!', icon='😍')
    st.download_button("Download Image", data=img_rm_dow, file_name=f"{file_name}_rembg.png", mime="image/png")


file_upload = st.sidebar.file_uploader("Choose a picture", type=['png', 'jpg', 'jpeg'])

if file_upload is not None:
    rm_bg(file_upload)
else:
    default_image_path = "./img/smile.jpg"
    if os.path.exists(default_image_path):
        default_load(default_image_path)
    else:
        st.warning("Default image not found. Please make sure the file path is correct.")
